#ifndef __UART_H_
#define __UART_H_

#if __cplusplus
extern "C" {
#endif

/* Initialiseer uart */
int uart_init(const char *dev);

/* Lees inkomend bericht. Voorzie buffer die lang genoeg is, daarin komt het inkomend bericht.
Het aantal gelezen bytes (in de buffer) wordt teruggegeven. */
int uart_read(char *buffer, int lengte);

/* Stuur een bericht. Geeft terug hoeveel bytes nog niet verstuurd werden.
Als 0 is alles verstuurd, gebruik uart_write_out() om de resterende bytes te versturen.
*/
int uart_write(char *bericht, int lengte);
/* Deze functie tracht de resterende bytes te versturen. Geeft ook het aantal resterende bytes terug. */
int uart_write_out();

#if __cplusplus
}
#endif

#endif
