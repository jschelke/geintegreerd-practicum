#ifndef RAINBOW_H
#define RAINBOW_H

class rainbowColor
{
    int counter = 0;
    bool inc = true;

public:
    rainbowColor (int offset)
    {
        counter = offset;
    }

    int next()
    {
        if (counter == 15)
            inc = false;
        if (counter == 0)
            inc = true;
        counter += (inc ? 1 : -1);

        return counter;
    }
};

class rainbow
{
    int x = 0;
    rainbowColor red = rainbowColor(0);
    rainbowColor green = rainbowColor(7);
    rainbowColor blue = rainbowColor(15);
    bool inc = true;

public:
    rainbow() {};

    virtual void drawLine(int x0, int y0, int x1, int y1, int color);

    void next()
    {
        int R = red.next();
        int G = green.next();
        int B = blue.next();

        if (x == 19)
            inc = false;
        if (x == 0)
            inc = true;
        x += (inc ? 1 : -1);

        int color = (R << 8) + (G << 4) + B;

        drawLine(x, 0, x, 9, color);
    }
};

#endif // RAINBOW_H
