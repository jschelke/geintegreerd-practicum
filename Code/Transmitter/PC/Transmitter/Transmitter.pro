TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
    UART.c \
    main.cpp

DISTFILES += \
    .gitignore

HEADERS += \
    UART.h \
    rainbow.h
