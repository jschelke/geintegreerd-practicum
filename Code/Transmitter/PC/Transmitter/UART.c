#include "UART.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <errno.h>
#include <termios.h>
#include <time.h>
#include <netdb.h>

#include <sys/stat.h>

#if __cplusplus
extern "C" {
#endif

#define BAUDRATE 			B115200

#define BUF_SIZE			2048

int UART_fd;
/* outgoing buffer */
int UART_begin;
int UART_end;
unsigned char buffer_out[BUF_SIZE];

/************************************************************************/
int uart_open(const char *dev);
void stty_telos();
void uart_send(unsigned char c);

/************************************************************************/
/*----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*/
int uart_init(const char *dev)
{
	int result;
	UART_begin=0;
	UART_end=0;
	result = uart_open(dev);
	stty_telos();

	return result;
}
/*----------------------------------------------------------------------*/
int uart_open(const char *dev)
{
	char t[32];
	strcpy(t, "/dev/");
	strncat(t, dev, sizeof(t) - 5);
	UART_fd=open(t, O_RDWR | O_NOCTTY | O_NONBLOCK);
	return (UART_fd>-1?1:0);
}
/*----------------------------------------------------------------------*/
void stty_telos()
{
	struct termios tty;
	speed_t speed = BAUDRATE;
	int i;

	if(tcflush(UART_fd, TCIOFLUSH) == -1) printf("tcflush error tty_telos();\n");

	if(tcgetattr(UART_fd, &tty) == -1) printf("tcgetattr error tty_telos();\n");

	cfmakeraw(&tty);
	tty.c_cflag &= ~CRTSCTS;
	tty.c_cflag &= ~HUPCL;
	tty.c_cflag &= ~CLOCAL;
	cfsetispeed(&tty, speed);
	cfsetospeed(&tty, speed);
	if(tcsetattr(UART_fd, TCSAFLUSH, &tty) == -1) printf("tcsetattr error tty_telos();\n");

#if 1
	/* Nonblocking read and write. */
	/* if(fcntl(fd, F_SETFL, O_NONBLOCK) == -1) err(1, "fcntl"); */

	tty.c_cflag |= CLOCAL;
	if(tcsetattr(UART_fd, TCSAFLUSH, &tty) == -1) printf("tcsetattr error tty_telos();\n");

	i = TIOCM_DTR;
	if(ioctl(UART_fd, TIOCMBIS, &i) == -1) printf("ioctl error tty_telos();\n");
#endif

	usleep(10*1000);		/* Wait for hardware 10ms. */

	/* Flush input and output buffers. */
	if(tcflush(UART_fd, TCIOFLUSH) == -1) printf("tcflush error tty_telos();\n");
	fcntl(UART_fd,F_SETFL,fcntl(UART_fd,F_GETFL)&~O_NONBLOCK); // added to avoid 100% CPU usage
}
/*************************** SENDING PART *******************************/
/*----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*/
/*void UART_write_char(unsigned char c)
{
	switch(c)
	{
		case UART_END:
			UART_send(UART_ESC);
			UART_send(UART_ESC_END);
			break;
		case UART_ESC:
			UART_send(UART_ESC);
			UART_send(UART_ESC_ESC);
			break;
		default:
			UART_send(c);
			break;
  }
}*/
/*----------------------------------------------------------------------*/
void uart_send(unsigned char c)
{
	if(UART_end >= BUF_SIZE)
	{
		printf("UART send buffer overflow\n");
	}
	buffer_out[UART_end] = c;
	UART_end++;
}
/*----------------------------------------------------------------------*/
int uart_isempty()
{
	return UART_end == 0;
}
/*----------------------------------------------------------------------*/
int uart_write_out()
{
	int n;

    if(uart_isempty())
	{
		return -1;
	}

	n = write(UART_fd, buffer_out + UART_begin, (UART_end - UART_begin));

	if(n == -1 && errno != EAGAIN)
	{
		printf("Failed to flush the outgoing UART buffer\n");
	}
	else if(n == -1)
	{
		printf("Error while writing to outgoing UART buffer\n");
	}
	else
	{
		UART_begin += n;
		if(UART_begin == UART_end)
		{
			UART_begin = UART_end = 0;
		}
	}
	return UART_end - UART_begin;
}
/*----------------------------------------------------------------------*/
int uart_write(char *inbuf, int len)
{
	u_int8_t *p = (u_int8_t*)inbuf;
	int i;

	//printf("Write packet to UART with length: %d \n", len);

	for(i = 0; i < len; i++)
	{
		uart_send(p[i]);
	}
	//UART_send(UART_END);
	return uart_write_out();
}
/*----------------------- RECEIVING AND PROCESSING ---------------------*/
/*----------------------------------------------------------------------*/
/*----------------------------------------------------------------------*/
int uart_read(char *buffer, int lengte)
{
	int numberRead = 0;
	int ret;
	
	while(numberRead < lengte)
	{
		ret = read(UART_fd, buffer + numberRead, 1 );
        if (ret == 0 || buffer[numberRead] == '\n')
		{
			return numberRead;
		}
		else if (ret<0)
		{
			printf("FATAL ERROR: Reading from UART failed read_UART()\n");
			return -1;
		}
		numberRead++;
    }
}

#if __cplusplus
}
#endif
