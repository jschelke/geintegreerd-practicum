#include <stdio.h>
#include <time.h>
#include <string.h>
#include <iostream>
#include "UART.h"
#include <unistd.h>
#include <cstring>
//#include "rainbow.h"

using namespace std;

//-- necessary declarations in order to run the application
#define uart_PORT		"ttyUSB0"

char buf[32] = "";
//rainbow Rainbow;

void sendMsg(char* buf, int len)
{
    uart_write(buf, len);
    while (uart_write_out() > 0); // loop tot de hele message verzonden is
}

void drawText(int x, int y, const char* string, int color)
{
    uint8_t red = (color & 0xF00) >> 8;
    uint8_t green = (color & 0xF0) >> 4;
    uint8_t blue = (color & 0xF);

    buf[1] = 's';   // signal we want to print a string
    buf[2] = red;
    buf[3] = green;
    buf[4] = blue;
    buf[5] = x;
    buf[6] = y;

    int len = strlen(string);
    memcpy(buf+7, string, len+1);   // include null character
    buf[len + 8] = '~';

    buf[0] = 9 + len;

    sendMsg(buf, buf[0]);
}

void fillScreen(int color)
{
    uint8_t red = (color & 0xF00) >> 8;
    uint8_t green = (color & 0xF0) >> 4;
    uint8_t blue = (color & 0xF);

    buf[0] = 6;
    buf[1] = 'F';   // signal we want to print a string
    buf[2] = red;
    buf[3] = green;
    buf[4] = blue;
    buf[5] = '~';

    sendMsg(buf, 6);
}

void drawPixel(int x, int y, int color)
{
    uint8_t red = (color & 0xF00) >> 8;
    uint8_t green = (color & 0xF0) >> 4;
    uint8_t blue = (color & 0xF);

    buf[0] = 8;
    buf[1] = 'p';   // signal we want to print a string
    buf[2] = red;
    buf[3] = green;
    buf[4] = blue;
    buf[5] = x;
    buf[6] = y;
    buf[7] = '~';

    sendMsg(buf, 8);
}

void drawLine(int x0, int y0, int x1, int y1, int color)
{
    uint8_t red = (color & 0xF00) >> 8;
    uint8_t green = (color & 0xF0) >> 4;
    uint8_t blue = (color & 0xF);

    buf[0] = 10;
    buf[1] = 'l';   // signal we want to print a string
    buf[2] = red;
    buf[3] = green;
    buf[4] = blue;
    buf[5] = x0;
    buf[6] = y0;
    buf[7] = x1;
    buf[8] = y1;
    buf[9] = '~';

    sendMsg(buf, 10);
}


//-------------------------------------------------------
int main(int argc, char* argv[])
{

    //--- making up and running
    if (!uart_init(uart_PORT))
    {
        printf("ERROR: making uart available\n");
        return -1;
    }

    printf("start\n");

    while(1)
    {
        fillScreen(0x000);
//        char test[] = "hallo";
//        drawText(1, 6, test, 0xf00);
//        drawPixel(18, 6, 0x010);
//        drawLine(0, 9, 17, 6, 0x0f0);
        drawText(1, 6, "B", 0x00F);
        drawText(5, 6, "e", 0x0F0);
        drawText(9, 6, "a", 0xF00);
        drawText(13, 6, "m", 0x555);
        drawLine(0, 7, 18, 7, 0xfff);
        drawLine(0, 2, 18, 2, 0xfff);
        drawLine(0, 2, 0, 7, 0xfff);
        drawLine(18, 2, 18, 7, 0xfff);
        drawLine(4, 2, 4, 7, 0xfff);
        drawLine(8, 2, 8, 7, 0xfff);
        drawLine(12, 2, 12, 7, 0xfff);
        usleep(1000000);

//        Rainbow.next();
//        usleep(2000);
    }

    return 0;
}

