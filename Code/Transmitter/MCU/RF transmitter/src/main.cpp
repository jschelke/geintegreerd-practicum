#include <Arduino.h>
#include "rfm7x.h"

byte number = 0;
char inBuffer[32] = {};
int pos = 0;

void setup()
{
    Serial.begin(serial_speed);
    pinMode(9, OUTPUT);
    digitalWrite(9, LOW);

    rfm7x_io_init();
    spi_init();

    while(!rfm7x_is_present()); // wait for end of rfm POR // it takes something about 16 ms
    Serial.println("RF present");

    rfm7x_init();
    
    _delay_ms(2); // >1,5ms startup delay 
    rfm7x_toggle_reg4(); // couldn't reproduce any "PLL is not locked" condition, but better to do it after all (probably have to be executed after every power up)
    _delay_ms(0.5); // probably not necessary, but it is said to be used

    rfm7x_mode_transmit();
}

void loop()
{
    if (Serial.available() > 0)
    {
        inBuffer[pos] = Serial.read();
        // Serial.print(inBuffer[pos]);

        if (inBuffer[pos] == '~')
        {
            while(rfm7x_tx_fifo_full()) // do not sent another packet until there is a space for it
            {
                uint8_t tmp = rfm7x_reg_read(RFM7x_REG_STATUS);
                if(tmp & RFM7x_STATUS_IRQ_MAX_RT)
                {
                    rfm7x_mode_transmit(); // have the same affect as the following 2 lines
                }
                Serial.println("fifo full");
            }

            rfm7x_transmit((uint8_t *)inBuffer, inBuffer[0]);

            // Serial.print(inBuffer[0], HEX);
            // Serial.print(inBuffer[1], HEX);
            // Serial.print(inBuffer[2], HEX);
            // Serial.print(inBuffer[3], HEX);
            // Serial.println(inBuffer[4], HEX);

            pos = -1;
            // memset(inBuffer, 0, 10);
        }
        pos = (pos < 32) ? pos + 1 : 0;
    }
}