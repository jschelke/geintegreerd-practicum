#include <Arduino.h>

#include <stdlib.h>
#include <string.h>

//avr headers
#include <avr/io.h>
#include <util/delay.h>

#include "rfm7x.h"

char buf[32];

struct pixel
{
    byte red : 4;
    byte green : 4;
    byte blue : 4;
};

pixel display[10][20] = {};

void setup() 
{
    Serial.begin(serial_speed);
    pinMode(9, OUTPUT);
    digitalWrite(9, LOW);
}

void loop(void)
{
    // uint32_t counter = 0;

    rfm7x_io_init();
    spi_init();

    while(!rfm7x_is_present()); // wait for end of rfm POR // it takes something about 16 ms
    Serial.println("RF present");

    rfm7x_init();
    
    _delay_ms(2); // >1,5ms startup delay 
    rfm7x_toggle_reg4(); // couldn't reproduce any "PLL is not locked" condition, but better to do it after all (probably have to be executed after every power up)
    _delay_ms(0.5); // probably not necessary, but it is said to be used

    rfm7x_mode_transmit();

    while (1)
    {
        for (byte row = 0; row < 10; row++)
        {
            for (byte column = 0; column < 20; column++)
            {
                display[row][column].red = row;
                display[row][column].green = row/2+column/2;
                display[row][column].blue = column/2;

                buf[0] = row;
                buf[1] = column;
                buf[2] = display[row][column].red;
                buf[3] = display[row][column].green;
                buf[4] = display[row][column].blue;

                // buf[0] = 0;
                // buf[1] = 0;
                // buf[2] = 1;
                // buf[3] = 1;
                // buf[4] = 1;


                // strcpy(buf,"counter: ");
                // ltoa(counter, (char*)&buf[9], 10);

                while(rfm7x_tx_fifo_full()) // do not sent another packet until there is a space for it
                {
                    uint8_t tmp = rfm7x_reg_read(RFM7x_REG_STATUS);
                    if(tmp & RFM7x_STATUS_IRQ_MAX_RT)
                    {
                        rfm7x_mode_transmit(); // have the same affect as the following 2 lines

                        //rfm7x_reg_write(RFM7x_REG_STATUS, 0x70); // clear all flags // nRF24l01+ and SI24R1 can live with this line
                        //rfm7x_reg_write(RFM7x_CMD_FLUSH_TX, 0); // but bk242x is "protected" from overrunning MAX_RT counter - FLUSH_TX have to be executed to unlock any further transmissions in AA mode
                    }
                    Serial.println("fifo full");
                }

                rfm7x_transmit((uint8_t *)buf, 5);
                // Serial.write(buf, strlen(buf));
                // Serial.write('\n');
                Serial.print(buf[0], HEX);
                Serial.print(buf[1], HEX);
                Serial.print(buf[2], HEX);
                Serial.print(buf[3], HEX);
                Serial.println(buf[4], HEX);

                _delay_ms(100);
                // counter++;
            }
        }

    }
}