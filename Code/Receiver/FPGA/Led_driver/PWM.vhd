----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    03:36:50 08/17/2019 
-- Design Name: 
-- Module Name:    PWM - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.utils.ALL;

----------------------------------------------------------------------------------
entity PWM is
	generic (
		clockDiv : natural
	);
	Port ( 
		clk : in  STD_LOGIC;
      PWM_vec : out  STD_LOGIC_VECTOR (16 downto 0) := (16 => '1', others => '0')
	);
end PWM;
----------------------------------------------------------------------------------

----------------------------------------------------------------------------------
architecture Behavioral of PWM is
	signal PWMClock : std_logic;
begin
	----------------------------------------------------------------------------------
	PWMClockDiv:process(clk)
		variable counter : UNSIGNED(log2(clockDiv/2)-1 downto 0) := (others=>'0');
		variable clk_signal : STD_LOGIC := '0';
	begin
		if (rising_edge(clk)) then
			if (counter = clockDiv/2) then
				counter := (others=>'0');
				clk_signal := not clk_signal;
				PWMClock <= clk_signal;
			else
				counter := counter+1;
			end if;
		end if;
	end process;
	----------------------------------------------------------------------------------
	
	----------------------------------------------------------------------------------
	PWMProc:process(PWMClock)
		variable counter : unsigned(3 downto 0) := (others => '0');
	begin
		if (rising_edge(PWMClock)) then
					
			if (counter = 0) then
				PWM_vec(1) <= '1';
			else
				PWM_vec(1) <= '0';
			end if;
			
			if (counter <= 1) then 
				PWM_vec(2) <= '1';
			else
				PWM_vec(2) <= '0';
			end if;
			
			if (counter <= 2) then 
				PWM_vec(3) <= '1';
			else
				PWM_vec(3) <= '0';
			end if;

			if (counter <= 3) then 
				PWM_vec(4) <= '1';
			else
				PWM_vec(4) <= '0';
			end if;

			if (counter <= 4) then 
				PWM_vec(5) <= '1';
			else
				PWM_vec(5) <= '0';
			end if;

			if (counter <= 5) then 
				PWM_vec(6) <= '1';
			else
				PWM_vec(6) <= '0';
			end if;

			if (counter <= 6) then 
				PWM_vec(7) <= '1';
			else
				PWM_vec(7) <= '0';
			end if;

			if (counter <= 7) then 
				PWM_vec(8) <= '1';
			else
				PWM_vec(8) <= '0';
			end if;

			if (counter <= 8) then 
				PWM_vec(9) <= '1';
			else
				PWM_vec(9) <= '0';
			end if;

			if (counter <= 9) then 
				PWM_vec(10) <= '1';
			else
				PWM_vec(10) <= '0';
			end if;

			if (counter <= 10) then 
				PWM_vec(11) <= '1';
			else
				PWM_vec(11) <= '0';
			end if;

			if (counter <= 11) then 
				PWM_vec(12) <= '1';
			else
				PWM_vec(12) <= '0';
			end if;

			if (counter <= 12) then 
				PWM_vec(13) <= '1';
			else
				PWM_vec(13) <= '0';
			end if;

			if (counter <= 13) then 
				PWM_vec(14) <= '1';
			else
				PWM_vec(14) <= '0';
			end if;

			if (counter <= 14) then 
				PWM_vec(15) <= '1';
			else
				PWM_vec(15) <= '0';
			end if;
			
			if (counter >= 15) then
				counter := (others=>'0');
			else
				counter := counter + 1;
			end if;
		end if;
	end process;
	----------------------------------------------------------------------------------

end Behavioral;
----------------------------------------------------------------------------------
