----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    21:39:27 08/16/2019 
-- Design Name: 
-- Module Name:    Main - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.utils.ALL;

----------------------------------------------------------------------------------
entity Main is
	generic(
		pixelClockDivider:natural := 20000;
		pixelPWMClockDivider:natural := 1250;
		display_width:natural := 20;
		display_height:natural := 10
	);
	
	port (
		clk:				IN	STD_LOGIC;
		Row_in: 			IN	STD_LOGIC_VECTOR(3 downto 0);
		Column_in:		IN	STD_LOGIC_VECTOR(4 downto 0);
		Red_in: 			IN	STD_LOGIC_VECTOR(3 downto 0);
		Green_in: 		IN	STD_LOGIC_VECTOR(3 downto 0);
		Blue_in: 		IN	STD_LOGIC_VECTOR(3 downto 0);
		Data_clock_in: IN	STD_LOGIC;
		Column_out: 	OUT STD_LOGIC_VECTOR(19 downto 0);
		Red_out: 		OUT STD_LOGIC_VECTOR(9 downto 0);
		Green_out: 		OUT STD_LOGIC_VECTOR(9 downto 0);
		Blue_out: 		OUT STD_LOGIC_VECTOR(9 downto 0)
	);
end Main;

----------------------------------------------------------------------------------
architecture Behavioral of Main is

	signal display : t_display(display_height-1 downto 0, display_width-1 downto 0);
	signal pixelClock : std_logic;
	signal pixelPWM : std_logic_vector(16 downto 0);
	signal currentColumn : t_Column;
	
	component PWM is
		generic (
			clockDiv : natural
		);
		Port ( 
			clk : in  STD_LOGIC;
			PWM_vec : out  STD_LOGIC_VECTOR (16 downto 0)
		);
	end component;	
	
begin
	----------------------------------------------------------------------------------
	PWM_generator:PWM 
		generic map (
			clockDiv => pixelPWMClockDivider
		)
		port map (
			clk => clk,
			PWM_vec => pixelPWM
		);
	
	Red_out(0) <= pixelPWM(display(0, currentColumn).red);
	Red_out(1) <= pixelPWM(display(1, currentColumn).red);
	Red_out(2) <= pixelPWM(display(2, currentColumn).red);
	Red_out(3) <= pixelPWM(display(3, currentColumn).red);
	Red_out(4) <= pixelPWM(display(4, currentColumn).red);
	Red_out(5) <= pixelPWM(display(5, currentColumn).red);
	Red_out(6) <= pixelPWM(display(6, currentColumn).red);
	Red_out(7) <= pixelPWM(display(7, currentColumn).red);
	Red_out(8) <= pixelPWM(display(8, currentColumn).red);
	Red_out(9) <= pixelPWM(display(9, currentColumn).red);

	Green_out(0) <= pixelPWM(display(0, currentColumn).green);
	Green_out(1) <= pixelPWM(display(1, currentColumn).green);
	Green_out(2) <= pixelPWM(display(2, currentColumn).green);
	Green_out(3) <= pixelPWM(display(3, currentColumn).green);
	Green_out(4) <= pixelPWM(display(4, currentColumn).green);
	Green_out(5) <= pixelPWM(display(5, currentColumn).green);
	Green_out(6) <= pixelPWM(display(6, currentColumn).green);
	Green_out(7) <= pixelPWM(display(7, currentColumn).green);
	Green_out(8) <= pixelPWM(display(8, currentColumn).green);
	Green_out(9) <= pixelPWM(display(9, currentColumn).green);

	Blue_out(0) <= pixelPWM(display(0, currentColumn).blue);
	Blue_out(1) <= pixelPWM(display(1, currentColumn).blue);
	Blue_out(2) <= pixelPWM(display(2, currentColumn).blue);
	Blue_out(3) <= pixelPWM(display(3, currentColumn).blue);
	Blue_out(4) <= pixelPWM(display(4, currentColumn).blue);
	Blue_out(5) <= pixelPWM(display(5, currentColumn).blue);
	Blue_out(6) <= pixelPWM(display(6, currentColumn).blue);
	Blue_out(7) <= pixelPWM(display(7, currentColumn).blue);
	Blue_out(8) <= pixelPWM(display(8, currentColumn).blue);
	Blue_out(9) <= pixelPWM(display(9, currentColumn).blue);
	
	----------------------------------------------------------------------------------
	updateArray:process(Data_clock_in)
		variable row : t_row;
		variable column : t_column;
	begin
		if (rising_edge(Data_clock_in)) then
			row := to_integer(unsigned(Row_in));
			column := to_integer(unsigned(Column_in));
			display(row, column).red	<= to_integer(unsigned(Red_in));
			display(row, column).green	<= to_integer(unsigned(Green_in));
			display(row, column).blue	<= to_integer(unsigned(Blue_in));
		end if;
	end process updateArray;
	----------------------------------------------------------------------------------
	
	----------------------------------------------------------------------------------
	driveDisplay:process(pixelClock)
	begin
		if (rising_edge(pixelClock)) then
		
			if (currentColumn = display_width - 1) then
				currentColumn <= 0;
			else
				currentColumn <= currentColumn + 1;
			end if;
			
			Column_out <= (others => '1');
			Column_out(currentColumn) <= '0';
			
			
		end if;
	end process driveDisplay;
	----------------------------------------------------------------------------------
	
	----------------------------------------------------------------------------------
	pixelClockDiv:process(clk)
		variable counter : UNSIGNED(log2(pixelClockDivider/2)-1 downto 0) := (others=>'0');
		variable clk_signal : STD_LOGIC := '0';
	begin
		if (rising_edge(clk)) then
			if (counter = pixelClockDivider/2) then
				counter := (others=>'0');
				clk_signal := not clk_signal;
				pixelClock <= clk_signal;
			else
				counter := counter+1;
			end if;
		end if;
	end process pixelClockDiv;
	----------------------------------------------------------------------------------
	
end Behavioral;
----------------------------------------------------------------------------------
