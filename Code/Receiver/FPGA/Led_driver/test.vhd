--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   04:28:52 08/17/2019
-- Design Name:   
-- Module Name:   /home/rapptor/Documents/GP/Led_matrix/Code/Receiver/FPGA/Led_driver/test.vhd
-- Project Name:  Led_driver
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: Main
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY test IS
END test;
 
ARCHITECTURE behavior OF test IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT Main
    PORT(
         clk : IN  std_logic;
         Row_in : IN  std_logic_vector(3 downto 0);
         Column_in : IN  std_logic_vector(4 downto 0);
         Red_in : IN  std_logic_vector(3 downto 0);
         Green_in : IN  std_logic_vector(3 downto 0);
         Blue_in : IN  std_logic_vector(3 downto 0);
         Data_clock_in : IN  std_logic;
         Column_out : OUT  std_logic_vector(19 downto 0);
         Red_out : OUT  std_logic_vector(9 downto 0);
         Green_out : OUT  std_logic_vector(9 downto 0);
         Blue_out : OUT  std_logic_vector(9 downto 0);
			test: out std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal Row_in : std_logic_vector(3 downto 0) := (others => '0');
   signal Column_in : std_logic_vector(4 downto 0) := (others => '0');
   signal Red_in : std_logic_vector(3 downto 0) := (others => '0');
   signal Green_in : std_logic_vector(3 downto 0) := (others => '0');
   signal Blue_in : std_logic_vector(3 downto 0) := (others => '0');
   signal Data_clock_in : std_logic := '0';

 	--Outputs
   signal Column_out : std_logic_vector(19 downto 0);
   signal Red_out : std_logic_vector(9 downto 0);
   signal Green_out : std_logic_vector(9 downto 0);
   signal Blue_out : std_logic_vector(9 downto 0);
	signal test: std_logic;

   -- Clock period definitions
   constant clk_period : time := 20 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: Main PORT MAP (
          clk => clk,
          Row_in => Row_in,
          Column_in => Column_in,
          Red_in => Red_in,
          Green_in => Green_in,
          Blue_in => Blue_in,
          Data_clock_in => Data_clock_in,
          Column_out => Column_out,
          Red_out => Red_out,
          Green_out => Green_out,
          Blue_out => Blue_out,
			 test => test
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;	

      wait for clk_period*10;

      -- insert stimulus here 

      wait;
   end process;

END;
