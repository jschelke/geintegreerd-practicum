/*
 * File:   main.cpp
 * Author: rapptor
 *
 * Created on August 9, 2019, 10:11 PM
 */


// DEVCFG3
#pragma config USERID = 0xFFFF          // Enter Hexadecimal value (Enter Hexadecimal value)
#pragma config PMDL1WAY = ON            // Peripheral Module Disable Configuration (Allow only one reconfiguration)
#pragma config IOL1WAY = ON             // Peripheral Pin Select Configuration (Allow only one reconfiguration)

// DEVCFG2
#pragma config FPLLIDIV = DIV_2         // PLL Input Divider (2x Divider)
#pragma config FPLLMUL = MUL_20         // PLL Multiplier (20x Multiplier)
#pragma config FPLLODIV = DIV_2         // System PLL Output Clock Divider (PLL Divide by 2)

// DEVCFG1
#pragma config FNOSC = FRCPLL           // Oscillator Selection Bits (Fast RC Osc with PLL)
#pragma config FSOSCEN = OFF            // Secondary Oscillator Enable (Disabled)
#pragma config IESO = OFF               // Internal/External Switch Over (Disabled)
#pragma config POSCMOD = OFF            // Primary Oscillator Configuration (Primary osc disabled)
#pragma config OSCIOFNC = OFF           // CLKO Output Signal Active on the OSCO Pin (Disabled)
#pragma config FPBDIV = DIV_1           // Peripheral Clock Divisor (Pb_Clk is Sys_Clk/8)
#pragma config FCKSM = CSDCMD           // Clock Switching and Monitor Selection (Clock Switch Disable, FSCM Disabled)
#pragma config WDTPS = PS4096           // Watchdog Timer Postscaler (1:1048576)
#pragma config WINDIS = OFF             // Watchdog Timer Window Enable (Watchdog Timer is in Non-Window Mode)
#pragma config FWDTEN = ON              // Watchdog Timer Enable (WDT Enabled)
#pragma config FWDTWINSZ = WINSZ_25     // Watchdog Timer Window Size (Window Size is 25%)

// DEVCFG0
#pragma config JTAGEN = OFF             // JTAG Enable (JTAG Disabled)
#pragma config ICESEL = ICS_PGx1        // ICE/ICD Comm Channel Select (Communicate on PGEC1/PGED1)
#pragma config PWP = OFF                // Program Flash Write Protect (Disable)
#pragma config BWP = OFF                // Boot Flash Write Protect bit (Protection Disabled)
#pragma config CP = OFF                 // Code Protect (Protection Disabled)

// #pragma config statements should precede project file includes.
// Use project enums instead of #define for ON and OFF.

#include <xc.h>
#include <string.h>
#include "settings.h"
#include "uart.h"
#include "time.h"
#include "./lib/RFM7x-lib-master/rfm7x.h"
#include "LEDMatrix.h"
#include "lib/Adafruit-GFX-Library/picopixel.h"

uint8_t buf[32];
uint8_t len;

LEDMatrix display;

int main(void) {
    // Uart for debugging
    initUART(113200);
    RPC5R = 1;  // Select Peripheral of pin C5 to uart TX
    TRISCbits.TRISC5 = 0;   // Set pin dir as output
    
    /* Init RF module */
    rfm7x_io_init();
    spi_init();
    
    while (!rfm7x_is_present()) // wait for end of rfm POR // it takes something about 16 ms
    {
//        SendChar('q');
        delay_us(500);
    }
    SendString("RF present\n");

    rfm7x_init();
    
    delay(2); // >1,5ms startup delay
    rfm7x_toggle_reg4(); // couldn't reproduce any "PLL is not locked" condition, but better to do it after all (probably have to be executed after every power up)
    delay_us(500); // probably not necessary, but it is said to be used

    // rfm7x_mode_transmit();
    rfm7x_mode_receive();
    
    /* setup display */
    display.fillScreen(0);
    display.setTextColor(0xFFF);
    display.setFont(&Picopixel);
    
//    display.setTextSize(1);

    while (1)
    {
        WDTCONbits.WDTCLR = 1;
        
        /* Listen to radio and relay data to the fpga */
        if ((len = rfm7x_receive(buf)))
        {            
            
            switch(buf[1])
            {
                case 's':
                {
                    int textColor = (buf[2] << 8) | (buf[3] << 4) | buf[4];
                    display.setTextColor(textColor);
                    display.setCursor(buf[5], buf[6]);
                    display.print((char*)buf+7);
                    break;
                }
                
                case 'F':
                {
                    SendString("here");
                    int color = (buf[2] << 8) | (buf[3] << 4) | buf[4];
                    display.fillScreen(color);
                    break;
                }
                
//                case 'R':
//                {
//                    int color = (buf[2] << 8) | (buf[3] << 4) | buf[4];
//                    display.fillRect(buf[5], buf[6], buf[7], buf[8], color);
//                    break;
//                }
                
//                case 'C':
//                {
//                    int color = (buf[2] << 8) | (buf[3] << 4) | buf[4];
//                    display.fillCircle(buf[5], buf[6], buf[7], color);
//                    break;
//                }
//                
//                case 'r':
//                {
//                    int color = (buf[2] << 8) | (buf[3] << 4) | buf[4];
//                    display.drawRect(buf[5], buf[6], buf[7], buf[8], color);
//                    break;
//                }
//                
//                case 'c':
//                {
//                    int color = (buf[2] << 8) | (buf[3] << 4) | buf[4];
//                    display.drawCircle(buf[5], buf[6], buf[7], color);
//                    break;
//                }
                
                case 'l':
                {
                    int color = (buf[2] << 8) | (buf[3] << 4) | buf[4];
                    display.drawLine(buf[5], buf[6], buf[7], buf[8], color);
                    break;
                }
                
                case 'p':
                {
                    int color = (buf[2] << 8) | (buf[3] << 4) | buf[4];
                    display.drawPixel(buf[5], buf[6], color);
                    break;
                }
            }
            
            for (int i = 0; i < len; i++)
                buf[i] += 48;    // map 0-20 binary to 1-? in ASCII
            buf[len] = '\n';
            buf[len + 1] = '\0';
            SendString((char*)buf);
            
            memset(buf, 0, 32);
                        
        }
    }
    
    return 0;
}
