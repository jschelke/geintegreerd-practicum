/* 
 * File:   LEDMatrix.h
 * Author: root
 *
 * Created on August 26, 2019, 12:46 AM
 */

#include <xc.h>
#include "lib/Adafruit-GFX-Library/Adafruit_GFX.h"

#ifndef LEDMATRIX_H
#define	LEDMATRIX_H

#define DISPWIDTH 20
#define DISPHEIGHT 10

class LEDMatrix : public Adafruit_GFX
{    
    char textBuf[32];
    
    public:
        LEDMatrix();
        void drawPixel(int16_t x, int16_t y, uint16_t color);
        void writeFastVLine(int16_t x0, int16_t y0, int16_t h, uint16_t color);
        void writeFastHLine(int16_t x0, int16_t y0, int16_t w, uint16_t color);
};

#define CLEAR_PORTA() {LATACLR = 0x790;}
#define CLEAR_PORTB() {LATBCLR = 0x3FE0;}  // 3FE0
#define CLEAR_PORTC() {LATCCLR = 0x3CF;}

#endif	/* LEDMATRIX_H */

