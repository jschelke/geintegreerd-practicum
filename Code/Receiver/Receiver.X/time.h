/* 
 * File:   time.h
 * Author: root
 *
 * Created on August 14, 2019, 9:52 PM
 */

#ifndef TIME_H
#define	TIME_H

void delay_us(unsigned int us);
void delay(unsigned int ms);


#endif	/* TIME_H */

