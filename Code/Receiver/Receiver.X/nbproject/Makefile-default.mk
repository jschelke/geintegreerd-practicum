#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-default.mk)" "nbproject/Makefile-local-default.mk"
include nbproject/Makefile-local-default.mk
endif
endif

# Environment
MKDIR=mkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=default
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=elf
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/Receiver.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/Receiver.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

ifeq ($(COMPARE_BUILD), true)
COMPARISON_BUILD=-mafrlcsj
else
COMPARISON_BUILD=
endif

ifdef SUB_IMAGE_ADDRESS

else
SUB_IMAGE_ADDRESS_COMMAND=
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=lib/Adafruit-GFX-Library/Adafruit_GFX.cpp lib/Adafruit-GFX-Library/glcdfont.c lib/Arduino/Print.cpp lib/RFM7x-lib-master/rfm7x.cpp lib/RFM7x-lib-master/rfm7x_hardware.cpp uart.cpp main.cpp time.cpp SPI.cpp LEDMatrix.cpp

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/lib/Adafruit-GFX-Library/Adafruit_GFX.o ${OBJECTDIR}/lib/Adafruit-GFX-Library/glcdfont.o ${OBJECTDIR}/lib/Arduino/Print.o ${OBJECTDIR}/lib/RFM7x-lib-master/rfm7x.o ${OBJECTDIR}/lib/RFM7x-lib-master/rfm7x_hardware.o ${OBJECTDIR}/uart.o ${OBJECTDIR}/main.o ${OBJECTDIR}/time.o ${OBJECTDIR}/SPI.o ${OBJECTDIR}/LEDMatrix.o
POSSIBLE_DEPFILES=${OBJECTDIR}/lib/Adafruit-GFX-Library/Adafruit_GFX.o.d ${OBJECTDIR}/lib/Adafruit-GFX-Library/glcdfont.o.d ${OBJECTDIR}/lib/Arduino/Print.o.d ${OBJECTDIR}/lib/RFM7x-lib-master/rfm7x.o.d ${OBJECTDIR}/lib/RFM7x-lib-master/rfm7x_hardware.o.d ${OBJECTDIR}/uart.o.d ${OBJECTDIR}/main.o.d ${OBJECTDIR}/time.o.d ${OBJECTDIR}/SPI.o.d ${OBJECTDIR}/LEDMatrix.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/lib/Adafruit-GFX-Library/Adafruit_GFX.o ${OBJECTDIR}/lib/Adafruit-GFX-Library/glcdfont.o ${OBJECTDIR}/lib/Arduino/Print.o ${OBJECTDIR}/lib/RFM7x-lib-master/rfm7x.o ${OBJECTDIR}/lib/RFM7x-lib-master/rfm7x_hardware.o ${OBJECTDIR}/uart.o ${OBJECTDIR}/main.o ${OBJECTDIR}/time.o ${OBJECTDIR}/SPI.o ${OBJECTDIR}/LEDMatrix.o

# Source Files
SOURCEFILES=lib/Adafruit-GFX-Library/Adafruit_GFX.cpp lib/Adafruit-GFX-Library/glcdfont.c lib/Arduino/Print.cpp lib/RFM7x-lib-master/rfm7x.cpp lib/RFM7x-lib-master/rfm7x_hardware.cpp uart.cpp main.cpp time.cpp SPI.cpp LEDMatrix.cpp



CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-default.mk dist/${CND_CONF}/${IMAGE_TYPE}/Receiver.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=32MX120F032D
MP_LINKER_FILE_OPTION=
# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assembleWithPreprocess
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/lib/Adafruit-GFX-Library/glcdfont.o: lib/Adafruit-GFX-Library/glcdfont.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/lib/Adafruit-GFX-Library" 
	@${RM} ${OBJECTDIR}/lib/Adafruit-GFX-Library/glcdfont.o.d 
	@${RM} ${OBJECTDIR}/lib/Adafruit-GFX-Library/glcdfont.o 
	@${FIXDEPS} "${OBJECTDIR}/lib/Adafruit-GFX-Library/glcdfont.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CPPC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/lib/Adafruit-GFX-Library/glcdfont.o.d" -o ${OBJECTDIR}/lib/Adafruit-GFX-Library/glcdfont.o lib/Adafruit-GFX-Library/glcdfont.c    -DXPRJ_default=$(CND_CONF)  $(COMPARISON_BUILD) 
	
else
${OBJECTDIR}/lib/Adafruit-GFX-Library/glcdfont.o: lib/Adafruit-GFX-Library/glcdfont.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/lib/Adafruit-GFX-Library" 
	@${RM} ${OBJECTDIR}/lib/Adafruit-GFX-Library/glcdfont.o.d 
	@${RM} ${OBJECTDIR}/lib/Adafruit-GFX-Library/glcdfont.o 
	@${FIXDEPS} "${OBJECTDIR}/lib/Adafruit-GFX-Library/glcdfont.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CPPC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/lib/Adafruit-GFX-Library/glcdfont.o.d" -o ${OBJECTDIR}/lib/Adafruit-GFX-Library/glcdfont.o lib/Adafruit-GFX-Library/glcdfont.c    -DXPRJ_default=$(CND_CONF)  $(COMPARISON_BUILD) 
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compileCPP
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/lib/Adafruit-GFX-Library/Adafruit_GFX.o: lib/Adafruit-GFX-Library/Adafruit_GFX.cpp  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/lib/Adafruit-GFX-Library" 
	@${RM} ${OBJECTDIR}/lib/Adafruit-GFX-Library/Adafruit_GFX.o.d 
	@${RM} ${OBJECTDIR}/lib/Adafruit-GFX-Library/Adafruit_GFX.o 
	@${FIXDEPS} "${OBJECTDIR}/lib/Adafruit-GFX-Library/Adafruit_GFX.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CPPC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c++ -c -mprocessor=$(MP_PROCESSOR_OPTION)  -frtti -fexceptions -fno-check-new -fenforce-eh-specs -O2 -MMD -MF "${OBJECTDIR}/lib/Adafruit-GFX-Library/Adafruit_GFX.o.d" -o ${OBJECTDIR}/lib/Adafruit-GFX-Library/Adafruit_GFX.o lib/Adafruit-GFX-Library/Adafruit_GFX.cpp   -DXPRJ_default=$(CND_CONF)  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/lib/Arduino/Print.o: lib/Arduino/Print.cpp  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/lib/Arduino" 
	@${RM} ${OBJECTDIR}/lib/Arduino/Print.o.d 
	@${RM} ${OBJECTDIR}/lib/Arduino/Print.o 
	@${FIXDEPS} "${OBJECTDIR}/lib/Arduino/Print.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CPPC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c++ -c -mprocessor=$(MP_PROCESSOR_OPTION)  -frtti -fexceptions -fno-check-new -fenforce-eh-specs -O2 -MMD -MF "${OBJECTDIR}/lib/Arduino/Print.o.d" -o ${OBJECTDIR}/lib/Arduino/Print.o lib/Arduino/Print.cpp   -DXPRJ_default=$(CND_CONF)  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/lib/RFM7x-lib-master/rfm7x.o: lib/RFM7x-lib-master/rfm7x.cpp  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/lib/RFM7x-lib-master" 
	@${RM} ${OBJECTDIR}/lib/RFM7x-lib-master/rfm7x.o.d 
	@${RM} ${OBJECTDIR}/lib/RFM7x-lib-master/rfm7x.o 
	@${FIXDEPS} "${OBJECTDIR}/lib/RFM7x-lib-master/rfm7x.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CPPC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c++ -c -mprocessor=$(MP_PROCESSOR_OPTION)  -frtti -fexceptions -fno-check-new -fenforce-eh-specs -O2 -MMD -MF "${OBJECTDIR}/lib/RFM7x-lib-master/rfm7x.o.d" -o ${OBJECTDIR}/lib/RFM7x-lib-master/rfm7x.o lib/RFM7x-lib-master/rfm7x.cpp   -DXPRJ_default=$(CND_CONF)  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/lib/RFM7x-lib-master/rfm7x_hardware.o: lib/RFM7x-lib-master/rfm7x_hardware.cpp  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/lib/RFM7x-lib-master" 
	@${RM} ${OBJECTDIR}/lib/RFM7x-lib-master/rfm7x_hardware.o.d 
	@${RM} ${OBJECTDIR}/lib/RFM7x-lib-master/rfm7x_hardware.o 
	@${FIXDEPS} "${OBJECTDIR}/lib/RFM7x-lib-master/rfm7x_hardware.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CPPC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c++ -c -mprocessor=$(MP_PROCESSOR_OPTION)  -frtti -fexceptions -fno-check-new -fenforce-eh-specs -O2 -MMD -MF "${OBJECTDIR}/lib/RFM7x-lib-master/rfm7x_hardware.o.d" -o ${OBJECTDIR}/lib/RFM7x-lib-master/rfm7x_hardware.o lib/RFM7x-lib-master/rfm7x_hardware.cpp   -DXPRJ_default=$(CND_CONF)  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/uart.o: uart.cpp  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/uart.o.d 
	@${RM} ${OBJECTDIR}/uart.o 
	@${FIXDEPS} "${OBJECTDIR}/uart.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CPPC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c++ -c -mprocessor=$(MP_PROCESSOR_OPTION)  -frtti -fexceptions -fno-check-new -fenforce-eh-specs -O2 -MMD -MF "${OBJECTDIR}/uart.o.d" -o ${OBJECTDIR}/uart.o uart.cpp   -DXPRJ_default=$(CND_CONF)  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/main.o: main.cpp  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/main.o.d 
	@${RM} ${OBJECTDIR}/main.o 
	@${FIXDEPS} "${OBJECTDIR}/main.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CPPC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c++ -c -mprocessor=$(MP_PROCESSOR_OPTION)  -frtti -fexceptions -fno-check-new -fenforce-eh-specs -O2 -MMD -MF "${OBJECTDIR}/main.o.d" -o ${OBJECTDIR}/main.o main.cpp   -DXPRJ_default=$(CND_CONF)  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/time.o: time.cpp  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/time.o.d 
	@${RM} ${OBJECTDIR}/time.o 
	@${FIXDEPS} "${OBJECTDIR}/time.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CPPC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c++ -c -mprocessor=$(MP_PROCESSOR_OPTION)  -frtti -fexceptions -fno-check-new -fenforce-eh-specs -O2 -MMD -MF "${OBJECTDIR}/time.o.d" -o ${OBJECTDIR}/time.o time.cpp   -DXPRJ_default=$(CND_CONF)  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/SPI.o: SPI.cpp  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/SPI.o.d 
	@${RM} ${OBJECTDIR}/SPI.o 
	@${FIXDEPS} "${OBJECTDIR}/SPI.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CPPC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c++ -c -mprocessor=$(MP_PROCESSOR_OPTION)  -frtti -fexceptions -fno-check-new -fenforce-eh-specs -O2 -MMD -MF "${OBJECTDIR}/SPI.o.d" -o ${OBJECTDIR}/SPI.o SPI.cpp   -DXPRJ_default=$(CND_CONF)  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/LEDMatrix.o: LEDMatrix.cpp  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/LEDMatrix.o.d 
	@${RM} ${OBJECTDIR}/LEDMatrix.o 
	@${FIXDEPS} "${OBJECTDIR}/LEDMatrix.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CPPC} $(MP_EXTRA_CC_PRE) -g -D__DEBUG   -fframe-base-loclist  -x c++ -c -mprocessor=$(MP_PROCESSOR_OPTION)  -frtti -fexceptions -fno-check-new -fenforce-eh-specs -O2 -MMD -MF "${OBJECTDIR}/LEDMatrix.o.d" -o ${OBJECTDIR}/LEDMatrix.o LEDMatrix.cpp   -DXPRJ_default=$(CND_CONF)  $(COMPARISON_BUILD) 
	
else
${OBJECTDIR}/lib/Adafruit-GFX-Library/Adafruit_GFX.o: lib/Adafruit-GFX-Library/Adafruit_GFX.cpp  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/lib/Adafruit-GFX-Library" 
	@${RM} ${OBJECTDIR}/lib/Adafruit-GFX-Library/Adafruit_GFX.o.d 
	@${RM} ${OBJECTDIR}/lib/Adafruit-GFX-Library/Adafruit_GFX.o 
	@${FIXDEPS} "${OBJECTDIR}/lib/Adafruit-GFX-Library/Adafruit_GFX.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CPPC} $(MP_EXTRA_CC_PRE)  -g -x c++ -c -mprocessor=$(MP_PROCESSOR_OPTION)  -frtti -fexceptions -fno-check-new -fenforce-eh-specs -O2 -MMD -MF "${OBJECTDIR}/lib/Adafruit-GFX-Library/Adafruit_GFX.o.d" -o ${OBJECTDIR}/lib/Adafruit-GFX-Library/Adafruit_GFX.o lib/Adafruit-GFX-Library/Adafruit_GFX.cpp   -DXPRJ_default=$(CND_CONF)  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/lib/Arduino/Print.o: lib/Arduino/Print.cpp  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/lib/Arduino" 
	@${RM} ${OBJECTDIR}/lib/Arduino/Print.o.d 
	@${RM} ${OBJECTDIR}/lib/Arduino/Print.o 
	@${FIXDEPS} "${OBJECTDIR}/lib/Arduino/Print.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CPPC} $(MP_EXTRA_CC_PRE)  -g -x c++ -c -mprocessor=$(MP_PROCESSOR_OPTION)  -frtti -fexceptions -fno-check-new -fenforce-eh-specs -O2 -MMD -MF "${OBJECTDIR}/lib/Arduino/Print.o.d" -o ${OBJECTDIR}/lib/Arduino/Print.o lib/Arduino/Print.cpp   -DXPRJ_default=$(CND_CONF)  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/lib/RFM7x-lib-master/rfm7x.o: lib/RFM7x-lib-master/rfm7x.cpp  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/lib/RFM7x-lib-master" 
	@${RM} ${OBJECTDIR}/lib/RFM7x-lib-master/rfm7x.o.d 
	@${RM} ${OBJECTDIR}/lib/RFM7x-lib-master/rfm7x.o 
	@${FIXDEPS} "${OBJECTDIR}/lib/RFM7x-lib-master/rfm7x.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CPPC} $(MP_EXTRA_CC_PRE)  -g -x c++ -c -mprocessor=$(MP_PROCESSOR_OPTION)  -frtti -fexceptions -fno-check-new -fenforce-eh-specs -O2 -MMD -MF "${OBJECTDIR}/lib/RFM7x-lib-master/rfm7x.o.d" -o ${OBJECTDIR}/lib/RFM7x-lib-master/rfm7x.o lib/RFM7x-lib-master/rfm7x.cpp   -DXPRJ_default=$(CND_CONF)  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/lib/RFM7x-lib-master/rfm7x_hardware.o: lib/RFM7x-lib-master/rfm7x_hardware.cpp  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/lib/RFM7x-lib-master" 
	@${RM} ${OBJECTDIR}/lib/RFM7x-lib-master/rfm7x_hardware.o.d 
	@${RM} ${OBJECTDIR}/lib/RFM7x-lib-master/rfm7x_hardware.o 
	@${FIXDEPS} "${OBJECTDIR}/lib/RFM7x-lib-master/rfm7x_hardware.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CPPC} $(MP_EXTRA_CC_PRE)  -g -x c++ -c -mprocessor=$(MP_PROCESSOR_OPTION)  -frtti -fexceptions -fno-check-new -fenforce-eh-specs -O2 -MMD -MF "${OBJECTDIR}/lib/RFM7x-lib-master/rfm7x_hardware.o.d" -o ${OBJECTDIR}/lib/RFM7x-lib-master/rfm7x_hardware.o lib/RFM7x-lib-master/rfm7x_hardware.cpp   -DXPRJ_default=$(CND_CONF)  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/uart.o: uart.cpp  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/uart.o.d 
	@${RM} ${OBJECTDIR}/uart.o 
	@${FIXDEPS} "${OBJECTDIR}/uart.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CPPC} $(MP_EXTRA_CC_PRE)  -g -x c++ -c -mprocessor=$(MP_PROCESSOR_OPTION)  -frtti -fexceptions -fno-check-new -fenforce-eh-specs -O2 -MMD -MF "${OBJECTDIR}/uart.o.d" -o ${OBJECTDIR}/uart.o uart.cpp   -DXPRJ_default=$(CND_CONF)  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/main.o: main.cpp  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/main.o.d 
	@${RM} ${OBJECTDIR}/main.o 
	@${FIXDEPS} "${OBJECTDIR}/main.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CPPC} $(MP_EXTRA_CC_PRE)  -g -x c++ -c -mprocessor=$(MP_PROCESSOR_OPTION)  -frtti -fexceptions -fno-check-new -fenforce-eh-specs -O2 -MMD -MF "${OBJECTDIR}/main.o.d" -o ${OBJECTDIR}/main.o main.cpp   -DXPRJ_default=$(CND_CONF)  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/time.o: time.cpp  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/time.o.d 
	@${RM} ${OBJECTDIR}/time.o 
	@${FIXDEPS} "${OBJECTDIR}/time.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CPPC} $(MP_EXTRA_CC_PRE)  -g -x c++ -c -mprocessor=$(MP_PROCESSOR_OPTION)  -frtti -fexceptions -fno-check-new -fenforce-eh-specs -O2 -MMD -MF "${OBJECTDIR}/time.o.d" -o ${OBJECTDIR}/time.o time.cpp   -DXPRJ_default=$(CND_CONF)  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/SPI.o: SPI.cpp  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/SPI.o.d 
	@${RM} ${OBJECTDIR}/SPI.o 
	@${FIXDEPS} "${OBJECTDIR}/SPI.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CPPC} $(MP_EXTRA_CC_PRE)  -g -x c++ -c -mprocessor=$(MP_PROCESSOR_OPTION)  -frtti -fexceptions -fno-check-new -fenforce-eh-specs -O2 -MMD -MF "${OBJECTDIR}/SPI.o.d" -o ${OBJECTDIR}/SPI.o SPI.cpp   -DXPRJ_default=$(CND_CONF)  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/LEDMatrix.o: LEDMatrix.cpp  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/LEDMatrix.o.d 
	@${RM} ${OBJECTDIR}/LEDMatrix.o 
	@${FIXDEPS} "${OBJECTDIR}/LEDMatrix.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CPPC} $(MP_EXTRA_CC_PRE)  -g -x c++ -c -mprocessor=$(MP_PROCESSOR_OPTION)  -frtti -fexceptions -fno-check-new -fenforce-eh-specs -O2 -MMD -MF "${OBJECTDIR}/LEDMatrix.o.d" -o ${OBJECTDIR}/LEDMatrix.o LEDMatrix.cpp   -DXPRJ_default=$(CND_CONF)  $(COMPARISON_BUILD) 
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/Receiver.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CPPC} $(MP_EXTRA_LD_PRE) -g   -mprocessor=$(MP_PROCESSOR_OPTION)  -o dist/${CND_CONF}/${IMAGE_TYPE}/Receiver.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}          -DXPRJ_default=$(CND_CONF)  $(COMPARISON_BUILD)   -mreserve=data@0x0:0x1FC -mreserve=boot@0x1FC00490:0x1FC00BEF  -Wl,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION),--defsym=__MPLAB_DEBUG=1,--defsym=__DEBUG=1,-D=__DEBUG_D,--defsym=_min_heap_size=200,--defsym=_min_stack_size=100,--no-code-in-dinit,--no-dinit-in-serial-mem,-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map",--memorysummary,dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml
	
else
dist/${CND_CONF}/${IMAGE_TYPE}/Receiver.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CPPC} $(MP_EXTRA_LD_PRE)  -mprocessor=$(MP_PROCESSOR_OPTION)  -o dist/${CND_CONF}/${IMAGE_TYPE}/Receiver.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}          -DXPRJ_default=$(CND_CONF)  $(COMPARISON_BUILD)  -Wl,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION),--defsym=_min_heap_size=200,--defsym=_min_stack_size=100,--no-code-in-dinit,--no-dinit-in-serial-mem,-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map",--memorysummary,dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml
	${MP_CC_DIR}/xc32-bin2hex dist/${CND_CONF}/${IMAGE_TYPE}/Receiver.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} 
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/default
	${RM} -r dist/default

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell "${PATH_TO_IDE_BIN}"mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
