#include "SPI.h"
#include "settings.h"
#include <math.h>
#include "uart.h"

void initSPI(uint32_t speed, SPIclockSourceSelect clockSource)
{    
    IEC1bits.SPI1EIE = 0;       // SPI interrupts disabled
    IEC1bits.SPI1RXIE = 0;
    IEC1bits.SPI1TXIE = 0;
     
    SPI1CONbits.ON = 0;         // Turn off SPI module
     
    SPI1BUF = 0;                // Clear the receive buffer
    
    if (clockSource = PBCLOCK)
    {
        SPI1CONbits.MCLKSEL = 0;    // SPI Clock source select
        SPI1BRG = F_CPU / PB_DIV / (2 * speed) - 1; // Clock devider
    }
    else    // REFCLK
    {
        SPI1CONbits.MCLKSEL = 1;    // Set SPI clock source to REFCLK out
        REFOCONbits.ROSEL = 0;      // Set REFCLK in to system PLL
        REFOCONbits.RODIV = F_CPU / (2 * speed);      // REFCLK clock divider
        REFOTRIMbits.ROTRIM = fmod((float)F_CPU / (2 * speed), 1) * 512 + 0.5;    // REFCLK clock trim
        REFOCONbits.ON = 1;         // Turn REFCLK module on
        REFOCONbits.RSLP = 0;       // Turn REFCLK module sleep mode off
    }    
     
    SPI1STATbits.SPIROV = 0;    // Clear overflow flag
     
     
    /* SPI1CON settings */
    SPI1CONbits.FRMEN = 0;      // Framed SPI support is disabled
    SPI1CONbits.SIDL = 0;       // Continue operation in IDLE mode
    SPI1CONbits.DISSDO = 0;     // SDO1 pin is controlled by the module
    SPI1CON2bits.AUDEN = 0;     // 8 bit mode
    SPI1CONbits.MODE16 = 0;     
    SPI1CONbits.MODE32 = 0;
    SPI1CONbits.CKP = 0;        // Idle state for clock is low, active state is high
    SPI1CONbits.CKE = 1;        // Output data changes on transition from active to idle
    SPI1CONbits.SMP = 1;        // Input data sampled at the end of data output time
    SPI1CONbits.SSEN = 0;       // Not in slave mode
    SPI1CONbits.MSTEN = 1;      // Master mode
     
    SPI1CONbits.ON = 1;         // Turn module on
}

uint8_t SPIWriteRead(uint8_t i)
{
    SPI1BUF = i;                    // Write to buffer for transmission
    while (!SPI1STATbits.SPIRBF);   // Wait for transfer to be completed
//    uint32_t temp = SPI1BUF;
    return SPI1BUF;                 // Return the received value
}