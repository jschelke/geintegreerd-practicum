#include "time.h"
#include "settings.h"
#include <xc.h>

void delay_us(unsigned int us)
{
    // Convert microseconds us into how many clock ticks it will take
    us *= F_CPU / 1000000 / 2; // Core Timer updates every 2 ticks

//    uint32_t oldTime = _CP0_GET_COUNT();
//
//    while (us + oldTime > _CP0_GET_COUNT()); // Wait until Core Timer count reaches the number we calculated earlier
    _CP0_SET_COUNT(0);
    while (us > _CP0_GET_COUNT());
}

void delay(unsigned int ms)
{
    // Convert microseconds us into how many clock ticks it will take
    ms *= F_CPU / 1000 / 2; // Core Timer updates every 2 ticks

//    uint32_t oldTime = _CP0_GET_COUNT();
//
//    while (ms + oldTime > _CP0_GET_COUNT()); // Wait until Core Timer count reaches the number we calculated earlier
    _CP0_SET_COUNT(0);
    while (ms > _CP0_GET_COUNT());
}