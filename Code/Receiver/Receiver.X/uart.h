/* 
 * File:   uart.h
 * Author: root
 *
 * Created on August 14, 2019, 2:13 AM
 */

#ifndef UART_H
#define	UART_H

void initUART(int buadRate);
void SendChar(char c);
void SendString(char *string);
char ReadChar(void);
void printBin(const int val);

#endif	/* UART_H */

