#include "LEDMatrix.h"
#include <xc.h>
#include "lib/Adafruit-GFX-Library/Adafruit_GFX.h"
#include "time.h"

LEDMatrix::LEDMatrix() : Adafruit_GFX(DISPWIDTH, DISPHEIGHT){
    /* Pin config */
    CLEAR_PORTA();
    CLEAR_PORTB();
    CLEAR_PORTC();
    TRISACLR = 0x790;   // Set all used ports as outputs
    TRISBCLR = 0x3FE0;
    TRISCCLR = 0x3CF;
}

void LEDMatrix::drawPixel(__int16_t x, __int16_t y, __uint16_t color)
{
    CLEAR_PORTA();
    CLEAR_PORTB();
    CLEAR_PORTC();
    
    uint8_t red = (color & 0xF00) >> 8;
    uint8_t green = (color & 0xF0) >> 4;
    uint8_t blue = (color & 0xF);
            
    LATASET = blue << 7;
    LATBSET = (x << 5) | (y << 10);
    LATCSET = red | (green << 6);
    
    LATAbits.LATA4 = 1; // Toggle data clock pin
    LATAbits.LATA4 = 0;
    
//    delay_us(10);
}

void LEDMatrix::writeFastHLine(int16_t x0, int16_t y0, int16_t w, uint16_t color)
{
    for (int x = x0; x < (x0 + w); x++)
    {
        drawPixel(x, y0, color);
    }
}

void LEDMatrix::writeFastVLine(int16_t x0, int16_t y0, int16_t h, uint16_t color)
{
    for (int y = y0; y < (y0 + h); y++)
    {
        drawPixel(x0, y, color);
    }
}