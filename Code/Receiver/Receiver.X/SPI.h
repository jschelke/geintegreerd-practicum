/* 
 * File:   SPI.h
 * Author: root
 *
 * Created on August 14, 2019, 10:52 PM
 */

#include <xc.h>

#ifndef SPI_H
#define	SPI_H

enum SPIclockSourceSelect {
    PBCLOCK,
    REFCLK
};

void initSPI(uint32_t speed, SPIclockSourceSelect clockSource);
uint8_t SPIWriteRead(uint8_t i);

#endif	/* SPI_H */

